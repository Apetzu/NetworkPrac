#ifndef MATH_H
#define MATH_H

#include <cmath>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif
#ifndef M_PI_2
	#define M_PI_2 1.57079632679489661923
#endif
#ifndef M_PI_4
	#define M_PI_4 0.785398163397448309616
#endif

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	Vector2(float p);
	Vector2(const Vector2& v);

	float x_ = 0.0f;
	float y_ = 0.0f;

	bool operator==(const Vector2& v) const;
	bool operator!=(const Vector2& v) const;
	Vector2 operator+(const Vector2& v) const;
	Vector2 operator-(const Vector2& v) const;
	Vector2 operator*(const float& p) const;
	Vector2 operator/(const float& p) const;

	Vector2& operator=(const Vector2& v);
	Vector2& operator=(const float& p);
	Vector2& operator-();
	Vector2& operator+=(const Vector2& v);
	Vector2& operator-=(const Vector2& v);
	Vector2& operator*=(const float& p);
	Vector2& operator/=(const float& p);

	// This is NOT dot product or scalar multiplication
	Vector2 Scale(const Vector2& v) const;
	float Dot(const Vector2& v) const;
	float Cross(const Vector2& v) const;
	float Length() const;
	Vector2 Abs() const;
	Vector2 Normalize() const;
	Vector2 Rotate(float angle) const;

	void Print() const;

	static const Vector2 ZERO;
	static const Vector2 ONE;
	static const Vector2 RIGHT;
	static const Vector2 LEFT;
	static const Vector2 UP;
	static const Vector2 DOWN;
};

float Clamp(float x, float lower, float upper);
float DegToRad(float r);

#endif