#ifndef CLIENT_H
#define CLIENT_H
#include "Shared.h"

void Init();
void Update();
void UpdateClientData(ClientPackage& clientPackage);
void SendData(ClientPackage& clientPackage);
void ReceiveData();
void Render();

#endif