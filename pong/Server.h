#ifndef SERVER_H
#define SERVER_H

#include "Shared.h"
#include <string>
#include <vector>
#include <winsock2.h>
#include <SDL2/SDL_timer.h>

typedef struct
{
	float playerPos = SCREEN_SIZE_H.y_;
	float playerPosY1 = playerPos + PADDLE_SIZE_H.y_;
	float playerPosY2 = playerPos - PADDLE_SIZE_H.y_;
	unsigned short playerWins = 0;
	Uint64 lastTimeStamp = 0;
	sockaddr_in playerAddress;
} Player;

typedef struct
{
	Vector2 ballPos = Vector2(SCREEN_SIZE_H.x_, SCREEN_SIZE_H.y_);
	Vector2 ballDir = Vector2(1.0f, 0.0f);
} Ball;

typedef struct
{
	Player players[MAX_PLAYERS];
	int connectedClients = 0;
	Ball ball;

	bool gameQuit = false;
	HWND winWindow;
	Uint64 perfCounterFreq = SDL_GetPerformanceFrequency();
	Uint64 currentTime = SDL_GetPerformanceCounter();
	Uint64 lastTime = lastTime;
	double deltaTime = 0;
} GameVariables;

void Init();
void Update(GameVariables &gameVars, SOCKET &sock);
int ReceiveData(GameVariables& gameVars, SOCKET& sock, sockaddr_in& saFrom);
void SendData(GameVariables& gameVars, SOCKET& sock, sockaddr_in& saFrom);
void UpdatePlayer(Player& player, unsigned char& keys, double& deltaTime);
void UpdateBall(GameVariables& gameVars);
void YCollisionCheck(float collisionPoint, Ball& ball, Player& player);
void ResetGame(GameVariables& gameVars);

#endif