#ifndef SERVER

#include "Client.h"
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <climits>
#include <winsock2.h>
#include <ws2tcpip.h>
#ifndef SIMPLE2D_H
#define SIMPLE2D_H
#include <simple2d.h>
#endif

static S2D_Window* window;
static const Uint8* keyStates;
static ServerPackage* serverPackage = new ServerPackage();
static SOCKET sock;
static sockaddr_in saTo;
static int saToSize = sizeof(saTo);
static Uint64 lastTimeStamp = 0;
static S2D_Text* scoreText;
static int connectionStatus = 0;
static char hostAddressStr[22];

void Init()
{
	// Assigning address family
	memset((char*)&saTo, 0, sizeof(saTo));
	saTo.sin_family = AF_INET;

	// Getting IP and port from the user
	std::string input;
	bool succeed = false;
	std::cout << "Give an IP address (leaving empty = localhost): ";
	while (!succeed)
	{
		std::getline(std::cin, input);
		if (input.empty())
		{
			input = DEFAULT_IP;
		}
		int result = inet_pton(AF_INET, input.c_str(), &saTo.sin_addr.S_un.S_addr);
		if (result == 1)
		{
			succeed = true;
		}
		else if (result == 0)
		{
			std::cout << "Invalid IP address. Enter again: ";
		}
		else if (result == -1)
		{
			std::cout << "Error at inet_pton(): " << WSAGetLastError() << std::endl;
			return;
		}
	}
	succeed = false;
	unsigned long port = 0;
	std::cout << "Give a port number (leaving empty = 7777): ";
	while (!succeed)
	{
		std::getline(std::cin, input);
		if (input.empty())
		{
			input = DEFAULT_PORT;
		}

		port = strtoul(input.c_str(), NULL, 10);

		if (port == 0 || port > 65535)
		{
			std::cout << "Invalid port number. Enter again: ";
		}
		else if (port == ULONG_MAX)
		{
			std::cout << "Error at strtoul().\n";
			return;
		}
		else
		{
			succeed = true;
		}
	}
	saTo.sin_port = htons(static_cast<u_short>(port));

	sprintf(hostAddressStr, "%hhu.%hhu.%hhu.%hhu:%hu",
		saTo.sin_addr.S_un.S_un_b.s_b1,
		saTo.sin_addr.S_un.S_un_b.s_b2,
		saTo.sin_addr.S_un.S_un_b.s_b3,
		saTo.sin_addr.S_un.S_un_b.s_b4,
		saTo.sin_port);
	
	if (InitWinsock(sock))
	{
		return;
	}

	std::cout << "Socket created...\n";

	// Initialize server package struct
	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		serverPackage->playerPos[i] = SCREEN_SIZE_H.y_;
		serverPackage->playerWins[i] = 0;
	}

	// Initialize Simple 2D and text.
	// Simple 2D calls Update() function every frame
	window = S2D_CreateWindow("Pong", SCREEN_W, SCREEN_H, Update, Render, 0);
	scoreText = S2D_CreateText("rsc/battlenet.ttf", "0 - 0", 100);
	scoreText->x = static_cast<int>(SCREEN_SIZE_H.x_ - (scoreText->width / 2.0f));
	scoreText->y = 20;
	S2D_Show(window);

	// Disconnecting and cleanup
	if (shutdown(sock, SD_BOTH) == SOCKET_ERROR)
	{
		std::cout << "shutdown failed: " << WSAGetLastError() << std::endl;
	}
	closesocket(sock);
	WSACleanup();
	S2D_FreeWindow(window);
	delete(scoreText);
}

void Update()
{
	ClientPackage clientPackage;
	UpdateClientData(clientPackage);
	SendData(clientPackage);
	ReceiveData();
}

void UpdateClientData(ClientPackage &clientPackage)
{
	// Update key inputs
	keyStates = SDL_GetKeyboardState(0);
	if (keyStates[SDL_SCANCODE_UP])
	{
		clientPackage.keys |= KEY_UP;
	}
	if (keyStates[SDL_SCANCODE_DOWN])
	{
		clientPackage.keys |= KEY_DOWN;
	}
	if (keyStates[SDL_SCANCODE_ESCAPE])
	{
		S2D_Close(window);
	}

	// Give a timestamp to the sended package
	clientPackage.timeStamp = SDL_GetPerformanceCounter();
}

void SendData(ClientPackage& clientPackage)
{
	if (sendto(sock, (char*)&clientPackage, (int)sizeof(clientPackage), 0, (sockaddr*)&saTo, sizeof(saTo)) == SOCKET_ERROR)
	{
		std::cout << "send failed: " << WSAGetLastError() << std::endl;
		closesocket(sock);
		WSACleanup();
		return;
	}
}

void ReceiveData()
{
	// Receive the player and ball positions from the server
	char buffer[sizeof(ServerPackage)];
	int recvResult = recvfrom(sock, (char*)buffer, (int)sizeof(buffer), 0, (sockaddr*)&saTo, &saToSize);
	if (recvResult > 0)
	{
		if (connectionStatus == 0 || connectionStatus == C_STATUS_DISCONNECTED)
		{
			connectionStatus = C_STATUS_CONNECTED;
			std::cout << "Connected to " << hostAddressStr << "...\n";
		}

		ServerPackage* recvPackage = (ServerPackage*)buffer;
		// Skip the package if it's old
		if (recvPackage->timeStamp > lastTimeStamp)
		{
			lastTimeStamp = recvPackage->timeStamp;
			memcpy(serverPackage, recvPackage, sizeof(ServerPackage));

			char text[14];
			sprintf(text, "%hu - %hu", serverPackage->playerWins[0], serverPackage->playerWins[1]);
			S2D_SetText(scoreText, text);
		}
	}
	else if (recvResult == SOCKET_ERROR)
	{
		int error = WSAGetLastError();
		// If the server was unreachable, indicate that to the user
		if (error == WSAECONNRESET)
		{
			if (connectionStatus == 0 || connectionStatus == C_STATUS_CONNECTED)
			{
				connectionStatus = C_STATUS_DISCONNECTED;
				std::cout << "Disconnected from " << hostAddressStr << ". Trying to connect again...\n";
			}
		}
		else
		{
			std::cout << "receive failed with error: " << error << std::endl;
		}
	}
}

void Render()
{	
	S2D_DrawLine(PADDLE_MARGIN,
		serverPackage->playerPos[0] + PADDLE_SIZE_H.y_,
		PADDLE_MARGIN,
		serverPackage->playerPos[0] - PADDLE_SIZE_H.y_,
		PADDLE_W,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f);

	S2D_DrawLine(PLAYER_2_POS_X,
		serverPackage->playerPos[1] + PADDLE_SIZE_H.y_,
		PLAYER_2_POS_X,
		serverPackage->playerPos[1] - PADDLE_SIZE_H.y_,
		PADDLE_W,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f);

	S2D_DrawCircle(serverPackage->ballPos.x_, serverPackage->ballPos.y_, BALL_RADIUS, 8, 1.0f, 1.0f, 1.0f, 1.0f);

	S2D_DrawText(scoreText);
}

#endif