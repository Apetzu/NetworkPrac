#ifdef SERVER

#include "Server.h"
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>
#include <Windows.h>

static bool colCheckDone = false;

void Init()
{
	// Assigning address family and IP
	sockaddr_in saIn;
	memset((char*)&saIn, 0, sizeof(saIn));
	saIn.sin_family = AF_INET;
	saIn.sin_addr.S_un.S_addr = INADDR_ANY;

	// Getting port from the user
	bool succeed = false;
	unsigned long port = 0;
	std::string input;
	std::cout << "Give a port number (leaving empty = 7777): ";
	while (!succeed)
	{
		std::getline(std::cin, input);
		if (input.empty())
		{
			input = DEFAULT_PORT;
		}

		port = strtoul(input.c_str(), NULL, 10);

		if (port == 0 || port > 65535)
		{
			std::cout << "Invalid port number. Enter again: ";
		}
		else if (port == ULONG_MAX)
		{
			std::cout << "Error at strtoul().\n";
			return;
		}
		else
		{
			succeed = true;
		}
	}
	saIn.sin_port = htons(static_cast<u_short>(port));
	
	SOCKET sock;
	if (InitWinsock(sock))
	{
		return;
	}

	// Server needs to bind the socket
	if (bind(sock, (sockaddr*)&saIn, sizeof(saIn)) == SOCKET_ERROR) 
	{
		std::cout << "bind failed with error: " << WSAGetLastError() << std::endl;
		closesocket(sock);
		WSACleanup();
		return;
	}

	std::cout << "Server running in port " << port << "...\n";

	GameVariables* gameVars = new GameVariables();
	gameVars->winWindow = GetForegroundWindow();

	// Main loop
	while (!gameVars->gameQuit)
	{
		Update(*gameVars, sock);
	}

	// Disconnecting and cleanup
	if (shutdown(sock, SD_BOTH) == SOCKET_ERROR) 
	{
		std::cout << "shutdown failed: " << WSAGetLastError() << std::endl;
	}
	closesocket(sock);
	WSACleanup();
	delete(gameVars);
}

void Update(GameVariables& gameVars, SOCKET& sock)
{
	// Update delta time
	gameVars.currentTime = SDL_GetPerformanceCounter();
	gameVars.deltaTime = static_cast<double>(gameVars.currentTime - gameVars.lastTime) / static_cast<double>(gameVars.perfCounterFreq);
	if (gameVars.deltaTime >= MAX_DELTATIME)
	{
		gameVars.deltaTime = MAX_DELTATIME;
	}
	gameVars.lastTime = gameVars.currentTime;

	sockaddr_in saFrom;
	int recvResult = ReceiveData(gameVars, sock, saFrom);

	// Server update
	if (gameVars.connectedClients >= MAX_PLAYERS)
	{
		UpdateBall(gameVars);
	}

	// Send server game data to clients
	if (recvResult != SOCKET_ERROR)
	{
		SendData(gameVars, sock, saFrom);
	}

	if (GetForegroundWindow() == gameVars.winWindow)
	{
		if (GetAsyncKeyState(VK_ESCAPE))
		{
			gameVars.gameQuit = true;
		}
	}

}

int ReceiveData(GameVariables& gameVars, SOCKET& sock, sockaddr_in &saFrom)
{
	char buffer[sizeof(ClientPackage)];
	int saSize = sizeof(saFrom);
	// Wait to get data from clients
	int recvResult = recvfrom(sock, (char*)&buffer, (int)sizeof(buffer), 0, (sockaddr*)&saFrom, &saSize);
	if (recvResult > 0)
	{
		// Check if data package is from a player
		int playerIndex = 0;
		bool isPlayer = false;
		for (; playerIndex < MAX_PLAYERS; ++playerIndex)
		{
			u_long playerIP = gameVars.players[playerIndex].playerAddress.sin_addr.S_un.S_addr;
			u_short playerPort = gameVars.players[playerIndex].playerAddress.sin_port;

			
			// If client is already existing player, the player index of the client = playerIndex
			if (saFrom.sin_addr.S_un.S_addr == playerIP && saFrom.sin_port == playerPort)
			{
				isPlayer = true;
				break;
			}
			// If the client is a new player and there is space to join, add the player
			if (playerIP == 0 && playerPort == 0)
			{
				printf("Player %d connected from %hhu.%hhu.%hhu.%hhu:%hu.\n",
					playerIndex + 1,
					saFrom.sin_addr.S_un.S_un_b.s_b1,
					saFrom.sin_addr.S_un.S_un_b.s_b2,
					saFrom.sin_addr.S_un.S_un_b.s_b3,
					saFrom.sin_addr.S_un.S_un_b.s_b4,
					saFrom.sin_port);
				isPlayer = true;
				gameVars.players[playerIndex].playerAddress = saFrom;
				++gameVars.connectedClients;
				break;
			}
		}

		// If the game has already 2 connected clients, others will be spectators
		if (isPlayer)
		{
			ClientPackage* clientPackage = (ClientPackage*)buffer;

			// Skip the package if it's old
			if (clientPackage->timeStamp > gameVars.players[playerIndex].lastTimeStamp)
			{
				gameVars.players[playerIndex].lastTimeStamp = clientPackage->timeStamp;

				if (clientPackage->keys != 0)
				{
					UpdatePlayer(gameVars.players[playerIndex], clientPackage->keys, gameVars.deltaTime);
				}
			}
		}
	}
	else if (recvResult == SOCKET_ERROR)
	{
		std::cout << "receive failed with error: " << WSAGetLastError() << std::endl;
	}

	return recvResult;
}

void SendData(GameVariables& gameVars, SOCKET& sock, sockaddr_in& saFrom)
{
	ServerPackage serverPackage;

	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		serverPackage.playerPos[i] = gameVars.players[i].playerPos;
		serverPackage.playerWins[i] = gameVars.players[i].playerWins;
	}
	serverPackage.ballPos = gameVars.ball.ballPos;
	serverPackage.timeStamp = gameVars.currentTime;

	if (sendto(sock, (char*)&serverPackage, (int)sizeof(serverPackage), 0, (sockaddr*)&saFrom, sizeof(saFrom)) == SOCKET_ERROR)
	{
		std::cout << "send failed: " << WSAGetLastError() << std::endl;
		closesocket(sock);
		WSACleanup();
		return;
	}
}

void UpdatePlayer(Player &player, unsigned char &keys, double &deltaTime)
{
	if (keys == KEY_UP)
	{
		player.playerPos -= static_cast<float>(PADDLE_SPEED * deltaTime);
	}
	else if (keys == KEY_DOWN)
	{
		player.playerPos += static_cast<float>(PADDLE_SPEED * deltaTime);
	}
	player.playerPos = Clamp(player.playerPos, PADDLE_SIZE_H.y_, PADDLE_MAX_POS_Y);

	player.playerPosY1 = player.playerPos + PADDLE_SIZE_H.y_;
	player.playerPosY2 = player.playerPos - PADDLE_SIZE_H.y_;
}

void UpdateBall(GameVariables& gameVars)
{
	// Update ball position
	gameVars.ball.ballPos += gameVars.ball.ballDir * static_cast<float>(gameVars.deltaTime * BALL_SPEED);

	// Collision check
	if (gameVars.ball.ballPos.x_ < PLAYER_1_XCOL)
	{
		if (!colCheckDone)
		{
			colCheckDone = true;
			YCollisionCheck(PLAYER_1_XCOL, gameVars.ball, gameVars.players[0]);
		}
	}
	else if (gameVars.ball.ballPos.x_ > PLAYER_2_XCOL)
	{
		if (!colCheckDone)
		{
			colCheckDone = true;
			YCollisionCheck(PLAYER_2_XCOL, gameVars.ball, gameVars.players[1]);
		}
	}
	else
	{
		colCheckDone = false;
	}
	if (gameVars.ball.ballPos.y_ < BALL_RADIUS)
	{
		gameVars.ball.ballDir.y_ *= -1;
		gameVars.ball.ballPos.y_ = BALL_RADIUS;
	}
	else if (gameVars.ball.ballPos.y_ > BALL_MAX_YCOL)
	{
		gameVars.ball.ballDir.y_ *= -1;
		gameVars.ball.ballPos.y_ = BALL_MAX_YCOL;
	}

	// If the ball has gone over the paddle
	if (gameVars.ball.ballPos.x_ <= -BALL_DIAM)
	{
		++gameVars.players[1].playerWins;
		ResetGame(gameVars);
	}
	else if (gameVars.ball.ballPos.x_ >= OUT_OF_BOUNDS_X)
	{
		++gameVars.players[0].playerWins;
		ResetGame(gameVars);
	}
}

void YCollisionCheck(float collisionPoint, Ball& ball, Player& player)
{
	// If the ball has collided with the player's paddle,
	// flip movement vector according to the contact point
	if (ball.ballPos.y_ - BALL_RADIUS <= player.playerPosY1 &&
		ball.ballPos.y_ + BALL_RADIUS >= player.playerPosY2)
	{
		float angle = (ball.ballPos.y_ - player.playerPos) / PADDLE_SIZE_H.y_ * static_cast<float>(M_PI_4);
		Vector2 ballDir(ball.ballDir.x_ > 0.0f ? -1.0f : 1.0f, 0.0f);
		ball.ballDir = ballDir.Rotate(ball.ballDir.x_ > 0.0f ? angle : -angle);
		ball.ballPos.x_ = collisionPoint;
	}
}

void ResetGame(GameVariables& gameVars)
{
	gameVars.ball.ballPos = Vector2(SCREEN_SIZE_H.x_, SCREEN_SIZE_H.y_);
	gameVars.ball.ballDir = Vector2(1.0f, 0.0f);

	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		gameVars.players[i].playerPos = SCREEN_SIZE_H.y_;
		gameVars.players[i].playerPosY1 = gameVars.players[i].playerPos + PADDLE_SIZE_H.y_;
		gameVars.players[i].playerPosY2 = gameVars.players[i].playerPos - PADDLE_SIZE_H.y_;
	}
}

#endif