#include "Math.h"
#include <cstdio>
#include <algorithm>

Vector2::Vector2() : x_(0.0f), y_(0.0f) {}

Vector2::Vector2(float x, float y) : x_(x), y_(y) {}

Vector2::Vector2(float p) : x_(p), y_(p) {}

Vector2::Vector2(const Vector2& v) : x_(v.x_), y_(v.y_)
{

}

bool Vector2::operator==(const Vector2& v) const
{
	return x_ == v.x_ && y_ == v.y_;
}

bool Vector2::operator!=(const Vector2& v) const
{
	return x_ != v.x_ || y_ != v.y_;
}

Vector2 Vector2::operator+(const Vector2& v) const
{
	Vector2 temp(*this);
	return temp += v;
}

Vector2 Vector2::operator-(const Vector2& v) const
{
	Vector2 temp(*this);
	return temp -= v;
}

Vector2 Vector2::operator*(const float& p) const
{
	Vector2 temp(*this);
	return temp *= p;
}

Vector2 Vector2::operator/(const float& p) const
{
	Vector2 temp(*this);
	return temp /= p;
}

Vector2& Vector2::operator=(const Vector2& v)
{
	x_ = v.x_;
	y_ = v.y_;
	return *this;
}

Vector2& Vector2::operator=(const float& p)
{
	x_ = p;
	y_ = p;
	return *this;
}

Vector2& Vector2::operator-()
{
	x_ = -x_;
	y_ = -y_;
	return *this;
}

Vector2& Vector2::operator+=(const Vector2& v)
{
	x_ += v.x_;
	y_ += v.y_;
	return *this;
}

Vector2& Vector2::operator-=(const Vector2& v)
{
	x_ -= v.x_;
	y_ -= v.y_;
	return *this;
}

Vector2& Vector2::operator*=(const float& p)
{
	x_ *= p;
	y_ *= p;
	return *this;
}

Vector2& Vector2::operator/=(const float& p)
{
	x_ = x_ / p;
	y_ = y_ / p;
	return *this;
}

// This is NOT dot product or scalar multiplication
Vector2 Vector2::Scale(const Vector2& v) const
{
	return Vector2(x_ * v.x_, y_ * v.y_);
}

float Vector2::Dot(const Vector2& v) const
{
	return x_ * v.x_ + y_ * v.y_;
}

float Vector2::Cross(const Vector2& v) const
{
	return x_ * v.y_ - y_ * v.x_;
}

float Vector2::Length() const
{
	return sqrt(x_ * x_ + y_ * y_);
}

Vector2 Vector2::Abs() const
{
	return Vector2(std::abs(x_), std::abs(y_));
}

Vector2 Vector2::Normalize() const
{
	float d = Length();
	return Vector2(x_ / d, y_ / d);
}

void Vector2::Print() const
{
	printf("% -10.4f% -10.4f\n", x_, y_);
}

Vector2 Vector2::Rotate(float angle) const
{
	float sinAngle = sinf(angle);
	float cosAngle = cosf(angle);
	return Vector2(x_ * cosAngle + y_ * sinAngle, -x_ * sinAngle + y_ * cosAngle);
}

const Vector2 Vector2::ZERO(0, 0);
const Vector2 Vector2::ONE(1, 1);
const Vector2 Vector2::RIGHT(1, 0);
const Vector2 Vector2::LEFT(-1, 0);
const Vector2 Vector2::UP(0, 1);
const Vector2 Vector2::DOWN(0, -1);

float Clamp(float x, float lower, float upper)
{
	return std::max(lower, std::min(x, upper));
}

float DegToRad(float r)
{
	return static_cast<float>(M_PI / 180.0f * r);
}