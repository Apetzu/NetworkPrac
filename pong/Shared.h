#ifndef SHARED_H
#define SHARED_H

#include "Math.h"
#include <iostream>
#include <winsock2.h>
#include <SDL2/SDL_timer.h>

// Shared definitions

#define SCREEN_W 640
#define SCREEN_H 480
#define MAX_DELTATIME 1.0
#define PADDLE_W 5.0f
#define PADDLE_H 110.0f
#define PADDLE_MARGIN 50.0f
#define PADDLE_SPEED 600.0f
#define BALL_DIAM 10.0f
#define BALL_SPEED 300.0f
const Vector2 SCREEN_SIZE_H = Vector2(SCREEN_W / 2.0f, SCREEN_H / 2.0f);
const Vector2 PADDLE_SIZE_H = Vector2(PADDLE_W / 2.0f, PADDLE_H / 2.0f);
const float PADDLE_MAX_POS_Y = SCREEN_H - PADDLE_SIZE_H.y_;
const float BALL_RADIUS = BALL_DIAM / 2.0f;
const float OUT_OF_BOUNDS_X = SCREEN_W + BALL_DIAM;
const float PLAYER_2_POS_X = SCREEN_W - PADDLE_MARGIN;
const float PLAYER_1_XCOL = PADDLE_MARGIN + BALL_RADIUS;
const float PLAYER_2_XCOL = PLAYER_2_POS_X - BALL_RADIUS;
const float BALL_MAX_YCOL = SCREEN_H - BALL_RADIUS;
#define KEY_UP 1
#define KEY_DOWN 2

#define DEFAULT_PORT "7777"
#define DEFAULT_IP "127.0.0.1"
#define MAX_PLAYERS 2
#define C_STATUS_DISCONNECTED 1
#define C_STATUS_CONNECTED 2

// ----------------------------------------------------------
// Shared structs

typedef struct
{
	unsigned char keys = 0;
	Uint64 timeStamp = 0;
} ClientPackage;

typedef struct
{
	float playerPos[MAX_PLAYERS] = { 0 };
	unsigned short playerWins[MAX_PLAYERS] = { 0 };
	Vector2 ballPos = Vector2(SCREEN_SIZE_H.x_, SCREEN_SIZE_H.y_);
	Uint64 timeStamp = 0;
} ServerPackage;

// ----------------------------------------------------------
// Shared functions

bool InitWinsock(SOCKET& sock);

#endif