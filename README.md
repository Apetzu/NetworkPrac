# NetworkPrac

This project is Pong clone that uses BSD sockets through Winsock 2.2.

This is the final assigment for a network course.

## Building

Before you build the project, init and update git submodules with:

`git submodule update --init --recursive`

To build the project, open Visual Studio solution and build either Debug or Release version of the server and client programs. The project has only VS solution.

## Libraries

The project uses [Simple2D](https://github.com/simple2d/simple2d) library for window creation, updates, drawing and rendering.